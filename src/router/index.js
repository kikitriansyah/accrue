import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/pages/index'
// import cafeShop from '@/pages/cafeShop'
// import newCafe from '@/pages/newCafe'
// import cafePromo from '@/pages/cafePromo'
// import fruitBar from '@/pages/fruitBar'
// import merchantPage from '@/pages/merchantPage'
import notFound from '@/pages/notFound'
// import help from '@/pages/help'
// import policy from '@/pages/policy'
import contact from '@/pages/contact'

//User
import userIndex from '@/pages/user/dashboard/index'
import userReport from '@/pages/user/dashboard/report'
import userProfile from '@/pages/user/dashboard/profile'
import userFaq from '@/pages/user/dashboard/faq'

//Admin
import adminLogin from '@/pages/admin/adminLogin'
import adminIndex from '@/pages/admin/dashboard/index'
import adminProfile from '@/pages/admin/dashboard/profile'
// import adminCategory from '@/pages/admin/dashboard/category/index'
import adminIncome from '@/pages/admin/dashboard/income/index'
import adminExpense from '@/pages/admin/dashboard/expense/index'
import adminUser from '@/pages/admin/dashboard/user/index'
import adminChartUser from '@/pages/admin/dashboard/chart/user'
import adminChartIncome from '@/pages/admin/dashboard/chart/income'
import adminChartExpense from '@/pages/admin/dashboard/chart/expense'

Vue.use(Router)

const router = new Router({
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index,
      meta: {
        layout: 'landing',
        guest: true
      }
    },
    // {
    //   path: '/cafe-shop',
    //   name: 'cafeShop',
    //   component: cafeShop,
    //   meta: {
    //     layout: 'default'
    //   }
    // },
    // {
    //   path: '/new-cafe',
    //   name: 'newCafe',
    //   component: newCafe,
    //   meta: {
    //     layout: 'default'
    //   }
    // },
    // {
    //   path: '/cafe-promo',
    //   name: 'cafePromo',
    //   component: cafePromo,
    //   meta: {
    //     layout: 'default'
    //   }
    // },
    // {
    //   path: '/fruit-bar',
    //   name: 'fruitBar',
    //   component: fruitBar,
    //   meta: {
    //     layout: 'default'
    //   }
    // },
    // {
    //   path: '/merchant-page',
    //   name: 'merchantPage',
    //   component: merchantPage,
    //   meta: {
    //     layout: 'default'
    //   }
    // },
    // {
    //   path: '/help',
    //   name: 'help',
    //   component: help,
    //   meta: {
    //     layout: 'default'
    //   }
    // },
    // {
    //   path: '/policy',
    //   name: 'policy',
    //   component: policy,
    //   meta: {
    //     layout: 'default'
    //   }
    // },
    {
      path: '/contact',
      name: 'contact',
      component: contact,
      meta: {
        layout: 'default'
      }
    },
    /* ==================
    / User
    ==================== */
    { 
      path: '/dashboard/user',
      name: 'userIndex',
      component: userIndex,
      meta: {
        layout: 'userDashboard',
        requiresAuth: true,
        is_user : true
      },
    },
    { 
      path: '/dashboard/user/report',
      name: 'userReport',
      component: userReport,
      meta: {
        layout: 'userDashboard',
        requiresAuth: true,
        is_user : true
      },
    },
    { 
      path: '/dashboard/user/profile',
      name: 'userProfile',
      component: userProfile,
      meta: {
        layout: 'userDashboard',
        requiresAuth: true,
        is_user : true
      },
    },
    { 
      path: '/dashboard/user/faq',
      name: 'userFaq',
      component: userFaq,
      meta: {
        layout: 'userDashboard'
      },
    },

    /* ==================
    / Admin
    ==================== */
    {
      path: '/admin/login',
      name: 'adminLogin',
      component: adminLogin,
      meta: {
        layout: 'login',
        guest: true,
      }
    },
    { 
      path: '/dashboard/admin',
      name: 'adminIndex',
      component: adminIndex,
      meta: {
        layout: 'adminDashboard',
        requiresAuth: true,
        is_admin : true
      },
    },
    { 
      path: '/dashboard/admin/profile',
      name: 'adminProfile',
      component: adminProfile,
      meta: {
        layout: 'adminDashboard',
        requiresAuth: true,
        is_admin : true
      },
    },
    // { 
    //   path: '/dashboard/admin/category',
    //   name: 'adminCategory',
    //   component: adminCategory,
    //   meta: {
    //     layout: 'adminDashboard',
    //     requiresAuth: true,
    //     is_admin : true
    //   },
    // },
    { 
      path: '/dashboard/admin/income',
      name: 'adminIncome',
      component: adminIncome,
      meta: {
        layout: 'adminDashboard',
        requiresAuth: true,
        is_admin : true
      },
    },
    { 
      path: '/dashboard/admin/expense',
      name: 'adminIncome',
      component: adminExpense,
      meta: {
        layout: 'adminDashboard',
        requiresAuth: true,
        is_admin : true
      },
    },
    { 
      path: '/dashboard/admin/user',
      name: 'adminUser',
      component: adminUser,
      meta: {
        layout: 'adminDashboard',
        requiresAuth: true,
        is_admin : true
      },
    },
    { 
      path: '/dashboard/admin/chart-user',
      name: 'adminChartUser',
      component: adminChartUser,
      meta: {
        layout: 'adminDashboard',
        requiresAuth: true,
        is_admin : true
      },
    },
    { 
      path: '/dashboard/admin/chart-income',
      name: 'adminChartIncome',
      component: adminChartIncome,
      meta: {
        layout: 'adminDashboard',
        requiresAuth: true,
        is_admin : true
      },
    },
    { 
      path: '/dashboard/admin/chart-expense',
      name: 'adminChartExpense',
      component: adminChartExpense,
      meta: {
        layout: 'adminDashboard',
        requiresAuth: true,
        is_admin : true
      },
    },
    {
      path: '/404',
      name: '404',
      component: notFound,
      meta: {
        layout: 'default',
        guest: true,
      }
    },
    {
      path: '*',
      redirect: '404',
      meta: {
        layout: 'default',
        guest: true
      }
    }
  ]
})

router.beforeEach((to, from, next) => {

  const admin = localStorage.getItem('adminToken')
  const user = localStorage.getItem('userToken')

  if(to.matched.some(record => record.meta.requiresAuth)) {
    // if (!admin ) {
    //   next({
    //     path: '/',
    //     params: { nextUrl: to.fullPath }
    //   })
    // } 
    
    // Check admin middleware
    if(to.matched.some(record => record.meta.is_admin)) {
      if(admin){
        next()
      }
      else{
        next({
          path: '/',
          params: { nextUrl: to.fullPath }
        })
      }
    }
    // Check user middleware
    else if(to.matched.some(record => record.meta.is_user)) {
      if(user){
        next()
      }
      else{
        next({
          path: '/',
          params: { nextUrl: to.fullPath }
        })
      }
    }
    //...
    else {
      next()
    }   
  } 

  else if(to.matched.some(record => record.meta.guest)) {
    if(!admin || !user){
      next()
    }
    else{
      next({ name: 'landing'})
    }
  }
  
  else {
    next() 
  }
})

export default router