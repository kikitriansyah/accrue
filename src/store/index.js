import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'

Vue.use(Vuex)

const store = () => {
  return new Vuex.Store({
    state: {
      attempt: null,
      token: null,
    },
    actions,
    mutations,
    getters
  })
}
export default store