import Vue from 'vue'

export default {
  // SET ATTEMPT
  SET_ATTEMPT: (state, attempt) => {
    state.attempt = attempt || null
  },

  // SET TOKEN
  SET_TOKEN: (state, token) => {
    state.auth = token || null
  },
}