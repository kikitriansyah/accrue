// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
// import App from './App'
import router from './router'

import vClickOutside from 'v-click-outside'

import VueAwesomeSwiper from 'vue-awesome-swiper'

import VueProgressBar from 'vue-progressbar'

import BootstrapVue from 'bootstrap-vue'

import Vuetify from 'vuetify'

import { VueExtendLayout, layout } from 'vue-extend-layout'

// import the helper
import api from './helpers/api'

// import store
import store from './store';

import VueCurrencyFilter from 'vue-currency-filter'

// Import app style
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vuetify/dist/vuetify.min.css'
import './assets/scss/app.scss'
import 'swiper/dist/css/swiper.css'

Vue.config.productionTip = false

Vue.use(vClickOutside)

Vue.use(VueAwesomeSwiper)

Vue.use(VueProgressBar, {
  color: '#bc0002',
  failedColor: 'red',
  height: '4px'
})

Vue.use(Vuetify)

Vue.use(VueExtendLayout)

Vue.use(require('vue-moment'));

Vue.use(VueCurrencyFilter,
{
  symbol : 'Rp',
  thousandsSeparator: '.',
  fractionCount: 0,
  fractionSeparator: ',',
  symbolPosition: 'front',
  symbolSpacing: true
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  // components: { App },
  // template: '<App/>',
  ...layout
})
