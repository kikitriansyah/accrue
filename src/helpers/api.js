import axios from 'axios';

const api = axios.create({
    // baseURL: 'https://akkruproject.herokuapp.com',
    timeout: 10000,
    params: {} // do not remove this, its added to add params later in the config
});

api.interceptors.request.use(function (config) {
  api.defaults.headers.common['Content-Type'] = 'application/json';
  const adminToken = localStorage.getItem('adminToken');
  const userToken = localStorage.getItem('userToken');
  if(adminToken) {
    config.headers.Authorization = adminToken;
  } else if(userToken) {
    config.headers.Authorization = userToken;
  } else {
    //
  }
  return config;
}, function (error) {
    return Promise.reject(error)
})

export default api