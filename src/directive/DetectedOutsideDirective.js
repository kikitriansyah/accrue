import Vue from 'vue';

export const DetectedOutside = {
  priority: 700,
  bind () {
    let self  = this
    this.event = function (event) { 
      console.log('emitting event')
      self.vm.$emit(self.expression,event) 
    }
    this.el.addEventListener('click', this.stopProp)
    document.body.addEventListener('click',this.event)
  },
  
  unbind() {
    console.log('unbind')
    this.el.removeEventListener('click', this.stopProp)
    document.body.removeEventListener('click',this.event)
  },
  stopProp(event) {event.stopPropagation() }
}

// You can also make it available globally.
Vue.directive('detected-outside', DetectedOutside)