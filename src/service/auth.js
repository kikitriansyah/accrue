const users = [
  {
    username: 'admin',
    password: 'admin'
  },
  {
    username: 'guest',
    password: 'password'
  }
];

export default (username, password, cb) => {
  const status = users.some(record => {
    return record.username === username && record.password === password
  });

  if(status) {
    cb('SUCCESS', {username, password})
  } else {
    cb('FAILED', {username, password})
  }
}